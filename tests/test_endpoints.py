from falcon import testing
import pytest
from api.app import get_app
@pytest.fixture()
def client():
    # Assume the hypothetical `myapp` package has a function called
    # `create()` to initialize and return a `falcon.App` instance.
    return testing.TestClient(get_app())


def test_create_game(client):

    result = client.simulate_post('/games/new')
    assert result.status_code == 201
    assert "game_id" in result.json


def test_play_game(client):
    result = client.simulate_post('/games/new')
    game_id = result.json["game_id"]

    for _i in range(0, 5):
        result = client.simulate_post(f'/games/{game_id}/guess', json={"word": "hello"})
        assert result.status_code == 200
        assert "correct_word" not in result.json

    result = client.simulate_post(f'/games/{game_id}/guess', json={"word": "hello"})
    assert result.status_code == 200
    assert "correct_word" in result.json


def test_game_over(client):
    result = client.simulate_post('/games/new')
    game_id = result.json["game_id"]
    for _i in range(0, 6):
        client.simulate_post(f'/games/{game_id}/guess', json={"word": "hello"})
    result = client.simulate_post(f'/games/{game_id}/guess', json={"word": "hello"})
    assert result.status_code == 400