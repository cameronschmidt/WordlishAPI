from wsgiref.simple_server import make_server

import falcon
from .game import Games, GameOverException


class GamesResource:
    def __init__(self, games):
        self.games = games

    def on_post(self, req, resp):
        game = self.games.new_game()
        resp.media = {"game_id": game.id, "guesses_remaining": game.guesses_remaining}
        resp.status = falcon.HTTP_201


class GuessResource:
    def __init__(self, games):
        self.games = games

    def on_post(self, req, resp, game_id):
        game = self.games[game_id]
        word = req.media["word"]
        try:
            guess_result = game.guess(word)
        except GameOverException as goe:
            raise falcon.HTTPBadRequest(title="Game is already over") from goe
        resp.media = guess_result
        resp.status = falcon.HTTP_200


def get_app():
    app = falcon.App()

    # Instantiate the game collection
    games = Games()

    # Instantiate resources
    games_resource = GamesResource(games)
    guess_resource = GuessResource(games)

    # Define routes
    app.add_route('/games/new', games_resource)
    app.add_route('/games/{game_id}/guess', guess_resource)

    return app

