import json
import uuid
from random import choice


class GameOverException(Exception):
    """ Raised when a game is over and a guess is attempted"""
    pass


class Games:
    """ Games is a collection of Game objects, indexed by ID """
    def __init__(self):
        self.games = {}
        with open("words.json", "r") as words_f:
            self.wordlist = json.load(words_f)

    def __getitem__(self, game_id):
        return self.games[game_id]
    def new_game(self):
        """ Create a new game and return the id """
        word = choice(self.wordlist).upper()
        game = Game(word)
        self.games[game.id] = game
        return game


class Game:
    def __init__(self, word):
        self.id = str(uuid.uuid4())
        self.word = word
        self.guesses_remaining = 6
        self.incorrectly_guessed_letters = set()

    #staticmethod
    def _get_letter_correctness(self, word):
        """
        Letter correctedness represents the correctness of each letter in the
        guessed word.
        """
        letter_correctness = [None, None, None, None, None]
        # Characters from the correct word that are represented by the above.
        # This is used to prevent duplicate "wrong position" messages.
        represented_characters = [None, None, None, None, None]

        # Check for correct letters first.
        # This needs to be done in passes to handle the duplicate letter edge case.
        for i in range(0, 5):
            if word[i] == self.word[i]:
                letter_correctness[i] = "correct"
                represented_characters[i] = True
        for i in range(0, 5):
            if word[i] == self.word[i]:
                continue
            if word[i] not in self.word:
                self.incorrectly_guessed_letters.add(word[i])
                letter_correctness[i] = "incorrect"
            else:
                # If the letter is in the word, but in the wrong position,
                # only report that as many times as the letter appears in the word.
                indices = [ind for ind, x in enumerate(self.word) if x == word[i]]
                if all([represented_characters[ind] for ind in indices]):
                    letter_correctness[i] = "incorrect"
                else:
                    # Update the represented_characters list with the correct character this represents
                    unrepresented_index = next(ind for ind in indices if not represented_characters[ind])
                    represented_characters[unrepresented_index] = True
                    letter_correctness[i] = "wrong_position"
        return letter_correctness

    def guess(self, word):
        """ Process a guess and return the result """

        word = word.upper()
        if self.guesses_remaining == 0:
            raise GameOverException()

        # Correct!
        if word == self.word:
            return {"guess_is_correct": True,
                    "letter_correctness": ["correct" for _i in range(len(self.word))],
                    "incorrectly_guessed_letters": list(self.incorrectly_guessed_letters),
                    "guesses_remaining": 0}

        # Determine per-letter correctness
        letter_correctness = self._get_letter_correctness(word)

        self.guesses_remaining -= 1

        # Game over
        if self.guesses_remaining == 0:
            return {"guess_is_correct": False,
                    "letter_correctness": letter_correctness,
                    "incorrectly_guessed_letters": list(self.incorrectly_guessed_letters),
                    "guesses_remaining": 0,
                    "correct_word": self.word}
        # Otherwise, return in-progress game state.
        return {"guess_is_correct": False,
                "letter_correctness": letter_correctness,
                "incorrectly_guessed_letters": list(self.incorrectly_guessed_letters),
                "guesses_remaining": self.guesses_remaining}
