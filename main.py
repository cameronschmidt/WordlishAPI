from wsgiref.simple_server import make_server

from api.app import get_app

if __name__ == '__main__':
    app = get_app()
    with make_server('', 8000, app) as httpd:
        print('Serving on port 8000...')

        # Serve until process is killed
        httpd.serve_forever()