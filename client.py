import sys
import requests

def start_game():
    r = requests.post("http://localhost:8000/games/new")
    response = r.json()
    if r.status_code != 201:
        sys.exit("Failed to create game. Make sure to run app.py first")
    game_id = response["game_id"]
    guesses_remaining = response["guesses_remaining"]
    return game_id, guesses_remaining

def get_word():
    word = input("Enter a word: ")
    if len(word) != 5:
        print("Word must be 5 characters long")
        return get_word()
    return word

def main():
    game_id, guesses_remaining = start_game()
    while True:
        print(f"Guesses remaining: {guesses_remaining}")
        word = get_word()

        r = requests.post(f"http://localhost:8000/games/{game_id}/guess", json={"word": word})
        response = r.json()
        if response["guess_is_correct"]:
            print("Correct!")
            break

        for i in range(0, 5):
            if response["letter_correctness"][i] == "correct":
                print(f"{word[i]} is correct")
            elif response["letter_correctness"][i] == "wrong_position":
                print(f"{word[i]} is in the word, but in the wrong position")
            else:
                print(f"{word[i]} is not in the word")

        print(f"Incorrectly guessed letters: {response['incorrectly_guessed_letters']}")
        guesses_remaining = response["guesses_remaining"]

        if "correct_word" in response:
            print(f"Game over! The word was {response['correct_word']}")
            break

if __name__ == "__main__":
    main()